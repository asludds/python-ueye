import numpy as np
import uc480
import tkinter as tk
import time
import matplotlib.pyplot as plt

class ueye_cam():
    def __init__(self,subsample_x=1,subsample_y=1):
        self.resolution_x = 1024
        self.resolution_y = 1280
        self.frame_rate = 25
        self.gain = 1 #This must be an integer
        self.pixel_clock = 38 #The pixel clock rate in MHz
        self.subsample_x = subsample_x
        self.subsample_y = subsample_y
        self.devID = 1 #Check the camera devID to set this correctly.
        self.cam = uc480.uc480(subsample_x=self.subsample_x,subsample_y=self.subsample_y) #Connecting to the camera
        self.cam.connect(ID=self.devID,useDevID=True)
        self.set_subsampling()
        self.set_framerate() #Sets the framerate from self.frame_rate
        self.set_gain() #Sets the gain from self.gain
        self.set_pixelclock()
           
    '''
    This function sets the clock rate of the pixel read out. The parameter, self.pixel_clock is in MHz
    '''
    def set_pixelclock(self):
        #First we want to get all of the possible pixel clock rates
        #self.cam.get_pixel_clock_values()

        #First we will try to set the pixel clock.
        self.cam.set_pixel_clock(self.pixel_clock)
        #Now we are going to verify that the pixel clock has been set close to the specified value
        measured_pixel_clock = self.cam.get_pixel_clock(self.pixel_clock)
        assert(np.abs(measured_pixel_clock - self.pixel_clock) < 1) #Note that we use 1 here because I am assuming that there is no more than MHz difference. This choice was arbitrary tbh
        return

    '''
    The camera gives us access to subsampling techniques which allows us to sample faster because of the decreased memory bandwidth.
    This function sets the subsampling rate for the horizontal and vertical directions of the camera
    '''
    def set_subsampling(self):
        #First we will set the subsampling values
        self.cam.set_subsampling(self.subsample_x,self.subsample_y)
        time.sleep(1)
        (retx,rety) = self.cam.get_subsampling()
        print("Subsampling rate set. X factor:", retx, " Y factor:" , rety)
        #assert(retx == self.subsample_x)
        assert(rety == self.subsample_y)
        img = self.cam.acquire()
        #make sure the shape of the image matches the new subsampling
        print("Subsample test image shape:",img.shape)
        assert(self.resolution_x/retx == img.shape[0])
        assert(self.resolution_y/rety == img.shape[1])

    '''
    This function takes self.frame_rate and uses it as well as the cameras get_exposure, get_exposure_limits, set_exposure
    '''
    def set_framerate(self):
        #Framerate in milliseconds
        framerate_milliseconds = 1000 * 1./self.frame_rate
        #Clamp ourselves into the framerate limits and make sure we are a multiple of the exposure increment
        exposure_minimum_ms, exposure_maximum_ms, exposure_increment_ms = self.cam.get_exposure_limits()
        exposure = np.clip(framerate_milliseconds,exposure_minimum_ms,exposure_maximum_ms)

        exposure = exposure_minimum_ms

        #Find closest exposure to quantize into
        temp1 = (exposure - exposure_minimum_ms)/exposure_increment_ms
        quantized_exposure = exposure_minimum_ms + temp1 * exposure_increment_ms
        #TODO REMOVE THIS !!!!!!!!!!!!!!!!!!
        quantized_exposure = exposure_minimum_ms
        self.cam.set_exposure(quantized_exposure)

        #We want to verify that it is actually set
        assert(np.abs(self.cam.get_exposure() - quantized_exposure) < exposure_increment_ms)

        return True

    def set_gain(self):
        #Make sure that the gain is clamped into a reasonable range.
        min_gain,max_gain,gain_increment = self.cam.get_gain_limits() #Note that gain increment is hardcoded to be 1, implying that our gain has to be an integer
        assert type(self.gain) == int
        assert gain_increment == 1

        #Make sure we are within the bounds
        quantized_gain = np.clip(self.gain,min_gain,max_gain)

        self.cam.set_gain(quantized_gain)

        #We want to make sure it is actually set
        assert(self.cam.get_gain() == quantized_gain)

        return True

    '''
    This function displays the camera feed onto a monitor
    '''
    def display_video(self):
        #Create the plt.imshow frame
        im = plt.imshow(np.zeros((self.resolution_x,self.resolution_y)))
        try:
            while True:
                img = self.cam.acquire()
                im.set_data(img)
                plt.imshow(img)
                plt.pause(0.05)
        except KeyboardInterrupt:
            pass
        except tk.TclError:
            pass
        except Exception as e:
            print(e)
        self.cam.disconnect()

    def disconnect(self):
        self.cam.disconnect()

    '''
    This is the main function of the measurement camera.
    '''
    def measure_grid(self):
        measurement = self.cam.acquire(N = 1)

    def measure_effective_framerate(self):
        import time
        start = time.time()
        number_of_measurements = 0
        print("Press keyboard key to end. Ctrl + C does this btw.")
        try:
            while True:
                img = self.cam.acquire(N = 1)
                number_of_measurements += 1
        except KeyboardInterrupt:
            final = time.time()
            total_time = final-start
            print("Expected frame rate:",number_of_measurements/total_time)
            print("Average time in seconds = ", total_time/number_of_measurements)
        return

    
    

if __name__ == "__main__":
    cam = ueye_cam(subsample_x=8,subsample_y=8)
    
    #Testing the frame rate
    cam.measure_effective_framerate()

    cam.disconnect()