# python-ueye

A python firmware for ueye cameras. Current frameworks lack compatibility with python3 or are not open-source such as pyueye. 
This project builds on top of the work of https://github.com/ddietze/Py-Hardware-Support.
