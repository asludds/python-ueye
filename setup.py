from distutils.core import setup

setup(
  name='python-ueye',
  packages=['python-ueye'],
  version='1.0',
  description='A python library for interfacing with ueye cameras that works out of the box.',
  author='Alexander Sludds',
  author_email='asludds@mit.edu',
  url='https://gitlab.com/asludds/python-ueye', 
  download_url= 'https://gitlab.com/asludds/python-ueye.git',
  keywords = ['cameras', 'thorlabs'],
  classifiers = [],
)